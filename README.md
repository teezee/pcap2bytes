Copyright (c) 2012-2017 thomas.zink _at_ uni-konstanz _dot_ de

Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

# pcap2bytes

_pcap2bytes_ is a simple program that read pcap data, either form file or an interface, extracts the ip packet data and writes the plain bytes to an output file. We use this as a preprocessor for certain packet analysis tools.

Build:

	> sudo apt install libpcap-dev
	> make

Get help with:

	> sudo ./pcap2bytes -h
	thomas.zink@uni-konstanz.de
	exports pcap packet payload to binary file
	usage: pcap2bytes [options]
	options:
	-i		interface, device
	-r		pcap input file
	-w		pcap dump file (not yet implemented!)
	-p		don't put interface into promiscuous mode
	-s		snaplen, how many bytes to capture for each packet
	-c		count, how many packets to capture
	-m		timeout in ms
	-o		output file name

