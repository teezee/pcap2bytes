/*
Copyright (c) 2012-2017 thomas.zink_at_uni-konstanz_dot_de

Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
*/
#ifndef _OPTPAR_H_
#define _OPTPAR_H_

#include <getopt.h>
#include <arpa/inet.h>

typedef struct optarg_s {
	char * dev;							// interface
	uint16_t snaplen;				// bytes to capture (-s)
	const char * rfile;		// pcap input file name (-r)
	const char * wfile;	// pcap dump file name (-w)
	uint8_t promiscuous;		// promiscuity	(-p)
	uint16_t to_ms;					// read timeout in ms (-m)
	uint32_t count;					// number of packets to process
	const char * filter;		// remaining filter string
	const char * outfile;
} optarg_t;

optarg_t * parse_options (int argc, const char * argv[]);
void usage (void);
void options_print (optarg_t *p);
char *concat_argv(char **argv);

#endif