/*
Copyright (c) 2012-2017 thomas.zink_at_uni-konstanz_dot_de

Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
*/
#include <netinet/ether.h>
#include "arpa/inet.h"
#include "defines.h"
#include "packethandler.h"
#include "protocols.h"
#include <ctype.h>

void
ethernet_handler (
	u_char * args,
	const struct pcap_pkthdr *header,
	const u_char * packet)
{
	static u_int32_t numpacket = 0;
	static u_int8_t warning_used = 0;
	
	// get ethernet header
	ether_t * eth = (ether_t *) (packet);
	u_short ether_type = ntohs(eth->ether_type);
	
#if DEBUG >= 1
	printf("%i: %li,%li\n",numpacket,(long int)header->ts.tv_sec,(long int)header->ts.tv_usec);
	char * shost = ether_ntoa((struct ether_addr *)eth->ether_shost);
	printf("(ETH) %s > ", shost);
	char * dhost = ether_ntoa((struct ether_addr *)eth->ether_dhost);
	printf("%s ", dhost);
	printf("type: %x\n", ether_type);
#endif
	
	// check network layer proto and proceed
	switch (ether_type) {
		case ETHERTYPE_IP:
			ip_handler(args, header, (packet + ETHER_HDR_LEN));
			break;
		default:
			if (warning_used == 0) {
				fprintf(stderr,"WARNING: only IP supported\n");
				warning_used = 1;
			}
			break;
	}
	numpacket++;
}

void
chdlc_handler (u_char * args, const struct pcap_pkthdr *header, const u_char * packet)
{
	static u_int32_t numpacket = 0;
	static u_int8_t warning_used = 0;
	chdlc_t * chdlc = (chdlc_t *) (packet);

#if DEBUG >= 1	
	printf("%i: %li,%li\n",numpacket,(long int)header->ts.tv_sec,(long int)header->ts.tv_usec);
	printf("(chdlc) %c, %c, %i\n", chdlc->address, chdlc->control, ntohs(chdlc->code));
#endif

	switch (ntohs(chdlc->code)) {
		case CHDLC_TYPE_IP:
			ip_handler(args, header, (packet + CHDLC_HDRLEN));
			break;
		default:
			if (warning_used == 0) {
				fprintf(stderr,"WARNING: only IP supported\n");
				warning_used = 1;
			}
			break;
	}
	numpacket++;
}

void
ip_handler (u_char * args, const struct pcap_pkthdr *header, const u_char * ippacket)
{
	// assumes *packet points at the beginning of the IP packet!
	// only call for raw IP or after adjusting pointers in correct
	// frame_handler!
	static u_int8_t warning_used = 0;
	ip_t * ip = (ip_t *) (ippacket);
	size_t size_ip = (ip->ip_hl*4);
	void * segment = NULL;
	size_t size_segment = 0;
	size_t size_payload = 0;
	const u_char * payload;
	
#if DEBUG >= 1
	char sip[INET_ADDRSTRLEN], dip[INET_ADDRSTRLEN];
	inet_ntop(AF_INET, &(ip->ip_src), sip, INET_ADDRSTRLEN);
	inet_ntop(AF_INET, &(ip->ip_dst), dip, INET_ADDRSTRLEN);
	printf("(IP) %s >> %s p: %d\n", sip, dip, ip->ip_p);
	printf("(IP) tos 0x%x, len 0x%x, id 0x%x, off 0x%x, ttl 0x%x, p 0x%x\n", ip->ip_tos,ntohs(ip->ip_len),ntohs(ip->ip_id),ntohs(ip->ip_off), ip->ip_ttl, ip->ip_p);
#endif

	// check for fragmentation
	if ((ip->ip_off && IP_MF) == 1) {
		size_segment = 0;
		// yeah i know, "bad style" and stuff, idc..
		goto skip_ip_p;
	}

	switch (ip->ip_p) {
		case IPPROTO_TCP:
			segment = (tcp_t *) (ippacket + size_ip);
			size_segment = ((tcp_t *)segment)->th_off * 4;
#if DEBUG >= 1
			printf("(TCP) sport: %hu dport: %hu\n", ntohs(((tcp_t *)segment)->th_sport), ntohs(((tcp_t *)segment)->th_dport));
#endif
			break;
		case IPPROTO_UDP:
			segment = (udp_t *) (ippacket + size_ip);
			size_segment = 8;
#if DEBUG >= 1
			printf("(UDP) sport: %hu dport: %hu\n", ntohs(((udp_t *)segment)->uh_sport), ntohs(((udp_t *)segment)->uh_dport));
#endif
			break;
		default:
			if (warning_used == 0) {
				fprintf(stderr,"WARNING: only TCP / UDP supported\n");
				warning_used = 1;
			}
			return;
			break;
	}
skip_ip_p:
	size_payload = ntohs(ip->ip_len) - (size_ip + size_segment);
	payload = (ippacket + size_ip + size_segment);
#if DEBUG >= 1
	printf("(DATA Bytes) %d, 0x%x\n",(int)size_payload,(int)size_payload);
	print_payload(payload, (int)size_payload);
#endif
	
	if (size_payload > 0)
		print_payload_to_file(payload, (int)size_payload, (FILE *)args);
}

void
print_hex_ascii_line(const u_char *payload, int len, int offset)
{
	int i;
	int gap;
	const u_char *ch;

	/* offset */
	printf("%05d   ", offset);
	
	/* hex */
	ch = payload;
	for(i = 0; i < len; i++) {
		printf("%02x ", *ch);
		ch++;
		/* print extra space after 8th byte for visual aid */
		if (i == 7)
			printf(" ");
	}
	/* print space to handle line less than 8 bytes */
	if (len < 8)
		printf(" ");
	
	/* fill hex gap with spaces if not full line */
	if (len < 16) {
		gap = 16 - len;
		for (i = 0; i < gap; i++) {
			printf("   ");
		}
	}
	printf("   ");

	/* ascii (if printable) */
	ch = payload;
	for(i = 0; i < len; i++) {
		if (isprint(*ch))
			printf("%c", *ch);
		else
			printf(".");
		ch++;
	}
	printf("\n");
	return;
}

/*
 * print packet payload data (avoid printing binary data)
 */
void
print_payload(const u_char *payload, int len)
{

	int len_rem = len;
	int line_width = 16;			/* number of bytes per line */
	int line_len;
	int offset = 0;					/* zero-based offset counter */
	const u_char *ch = payload;

	if (len <= 0)
		return;

	/* data fits on one line */
	if (len <= line_width) {
		print_hex_ascii_line(ch, len, offset);
		return;
	}

	/* data spans multiple lines */
	for ( ;; ) {
		/* compute current line length */
		line_len = line_width % len_rem;
		/* print line */
		print_hex_ascii_line(ch, line_len, offset);
		/* compute total remaining */
		len_rem = len_rem - line_len;
		/* shift pointer to remaining bytes to print */
		ch = ch + line_len;
		/* add offset */
		offset = offset + line_width;
		/* check if we have line width chars or less */
		if (len_rem <= line_width) {
			/* print last line and get out */
			print_hex_ascii_line(ch, len_rem, offset);
			break;
		}
	}

	return;
}

void
print_payload_to_file(const u_char *payload, int len, FILE *file)
{
	int nprinted = 0;
	u_int nlen = htonl(len);
	const u_char *chr = (u_char*) &nlen;
	for (nprinted = 0; nprinted < 4; nprinted++,chr++)
		fprintf(file, "%c",*chr);
	chr = payload;
	for (nprinted = 0; nprinted < len; nprinted++, chr++)
		fprintf(file,"%c", *chr);
}

