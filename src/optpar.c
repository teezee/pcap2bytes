/*
Copyright (c) 2012-2017 thomas.zink_at_uni-konstanz_dot_de

Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
*/
#include "defines.h"
#include "optpar.h"
#include <getopt.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

char * concat_argv(char **argv)
{
	char **p;
	u_short len = 0;
	char *buf;
	char *src, *dst;
	
	p = argv;
	if (*p == 0)
		return 0;
	
	while (*p)
		len += strlen(*p++) + 1;
	
	buf = (char *)malloc(len);
	if (buf == NULL) {
		fprintf(stdout, "ERROR: could not allocate memory for argv concatenation!\n");
		exit(EXIT_FAILURE);
	}
	
	p = argv;
	dst = buf;
	while ((src = *p++) != NULL) {
		while ((*dst++ = *src++) != '\0') ;
		dst[-1] = ' ';
	}
	dst[-1] = '\0';
	
	return buf;
}


optarg_t * parse_options (int argc, const char * argv[])
{
	optarg_t *poptarg = (optarg_t *) malloc(sizeof(optarg_t));
	if (!poptarg) {
		fprintf(stderr, "ERROR: could not allocate memory for option parser!\n");
		exit(EXIT_FAILURE);
	}
	
	// set defaults
	poptarg->promiscuous = 1;
	poptarg->to_ms = 100;
	poptarg->snaplen = 100;
	poptarg->count = -1;
	
	// check arguments
	int c;
	while ((c = getopt(argc, (char**)argv, "s:r:o:w:m:i:c:p?h")) != -1) {
		switch (c) {
			case 's':
				poptarg->snaplen = atoi(optarg);
				break;
			case 'r':
				poptarg->rfile = optarg;
				break;
			case 'o':
				poptarg->outfile = optarg;
				break;
			case 'w':
				poptarg->wfile = optarg;
				break;
			case 'm':
				poptarg->to_ms = atoi(optarg);
				break;
			case 'p':
				poptarg->promiscuous = 0;
				break;
			case 'i':
				poptarg->dev = optarg;
				break;
			case 'c':
				poptarg->count = atoi(optarg);
				break;
			case '?':
				usage();
				break;
			case 'h':
				usage();
				break;
			default:
				usage();
				break;
		}
	}
	
	// set the remaining as filter string
	poptarg->filter = concat_argv((char**)&argv[optind]);
	return(poptarg);
}

void usage (void)
{
	printf(
		   "usage: pcap2bytes [options]\n"
		   "options:\n"
		   "-i\t\tinterface, device\n"
		   "-r\t\tpcap input file\n"
		   "-w\t\tpcap dump file (not yet implemented!)\n"
		   "-p\t\tdon't put interface into promiscuous mode\n"
		   "-s\t\tsnaplen, how many bytes to capture for each packet\n"
			 "-c\t\tcount, how many packets to capture\n"
		   "-m\t\ttimeout in ms\n"
		   "-o\t\toutput file name\n"
	);
	exit(EXIT_SUCCESS);
}

void options_print (optarg_t *p)
{
	printf(
		   "Using Options:\n"
		   "dev: %s\n"
		   "snaplen: %i\n"
		   "rfile: %s\n"
		   "wfile: %s\n"
		   "promiscuous: %i\n"
		   "to_ms: %i\n"
		   "filter: %s\n"
		   "outfile: %s\n",
			 p->dev,p->snaplen,
		   p->rfile,p->wfile,p->promiscuous,
			 p->to_ms,p->filter,p->outfile
	);
}