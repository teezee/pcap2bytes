/*
Copyright (c) 2012-2017 thomas.zink_at_uni-konstanz_dot_de

Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
*/
#include <signal.h>
#include <setjmp.h>
#include <stdlib.h>
#include <stdio.h>
#include <pcap.h>
#include <unistd.h>			// geteuid
#include "defines.h"
#include "optpar.h"
#include "packethandler.h"
#include "protocols.h"

void signal_handler (int sig);
void print_banner (void);
int sniff (optarg_t *optargs);

optarg_t * optargs;			// cmd opts
jmp_buf state;					// signalling state

int
main (int argc, const char **argv)
{
	print_banner();
	if (geteuid()) {
		printf("ERROR: must be root ... exiting\n");
		return EXIT_SUCCESS;
	}
	
	optargs = parse_options(argc, argv);
#if DEBUG >= 1
	options_print(optargs);
#endif
	sniff(optargs);
	
	return EXIT_SUCCESS;
}

void
signal_handler (int sig)
{
	signal(sig,signal_handler);
	switch (sig) {
		case SIGINT:
			longjmp(state, SIGINT);
			break;
		default:
			break;
	}
}

void
print_banner (void)
{
	printf("%s\n%s\n%s\n",
		   APP_NAME,EMAIL,TASK
	);
}

int
sniff (optarg_t *optargs)
{
	// some initial setup stuff
	pcap_t * handle;					// pcap session handle
	char *dev = optargs->dev;			// device to sniff on
	char errbuf[PCAP_ERRBUF_SIZE];		// error buffer
	struct bpf_program filter;			// compiled filter string
	bpf_u_int32 net, mask;				// network / netmask
	char snet[INET_ADDRSTRLEN], smask[INET_ADDRSTRLEN]; // string representations
	int dtl;							// data link type
	void *args = NULL;
	void (*callback) (u_char * args, const struct pcap_pkthdr *header, const u_char * packet);
	struct pcap_stat * stats = (struct pcap_stat *)malloc(sizeof(struct pcap_stat));
	if (optargs->outfile == NULL) {
		args = stdout;
	} else {
		args = fopen(optargs->outfile,"w");
	}

	// check interface
	if (dev == NULL && optargs->rfile == NULL) {
		printf("no interface or file specified, looking ...\n");
		dev = pcap_lookupdev(errbuf);
		if (dev == NULL) {
			fprintf(stderr, "ERROR: couldn't find default device: %s\n", errbuf);
			fprintf(stderr, "try running as root / superuser\n");
			exit(EXIT_FAILURE);
		}
		// find interface properties
		printf("check device properties ...\n");
		if (pcap_lookupnet(dev, &net, &mask, errbuf) == -1) {
			fprintf(stderr, "WARNING: couldn't get netmask for device %s: %s\n", dev, errbuf);
			net = 0;
			mask = 0;
		}
		inet_ntop(AF_INET, &(net), snet, INET_ADDRSTRLEN);
		inet_ntop(AF_INET, &(mask), smask, INET_ADDRSTRLEN);
		printf("INFO: using device %s, %s / %s\n", dev, snet, smask);
	}
		
	// open the session
	if (optargs->rfile == NULL) {
		handle = pcap_open_live(dev, BUFSIZ, optargs->promiscuous, optargs->snaplen, errbuf);
		if (handle == NULL) {
			fprintf(stderr, "ERROR: couldn't open device %s: %s\n", dev, errbuf);
			exit(EXIT_FAILURE);
		}		
	} else {
		handle = pcap_open_offline(optargs->rfile, errbuf);
		if (handle == NULL) {
			fprintf(stderr, "ERROR: couldn't open file %s: %s\n", optargs->rfile, errbuf);
			exit(EXIT_FAILURE);
		}
	}
	
	// check datalink type
	// and set callback function appropriately
	// these are defined in handler.{h,cpp}
	dtl = pcap_datalink(handle);
	switch (dtl) {
		case DLT_EN10MB:
			callback = &ethernet_handler;
			break;
		case DLT_CHDLC:
			callback = &chdlc_handler;
			break;
		case DLT_RAW:
			callback = &ip_handler;
			break;
		default:
			fprintf(stderr, "ERROR: dtl %d unsupported.\n", dtl);
			exit(EXIT_FAILURE);
			break;
	}
	
	// compile filter
	if (pcap_compile(handle, &filter, optargs->filter, 0, net) == -1) {
		fprintf(stderr, "ERROR: couldn't parse filter %s: %s\n", optargs->filter, pcap_geterr(handle));
		exit(EXIT_FAILURE);
	}
	
	// set filter
	if (pcap_setfilter(handle, &filter) == -1) {
		fprintf(stderr, "ERROR: couldn't install filter %s: %s\n", optargs->filter, pcap_geterr(handle));
		exit(EXIT_FAILURE);
	}
	
	// do the loop, while not SIGINT
	signal(SIGINT,signal_handler);
	if (setjmp(state)==0) {
		pcap_loop(handle, -1, callback, args);
	} 
	
	// clean up
	printf("\n\n\n\n\nshutting down ...\n");
	if (pcap_stats(handle, stats)==-1) {
		printf("not captured anything\n");
	} else {
		printf("%d packets received\n%d packets dropped\n",stats->ps_recv,stats->ps_drop);
	}
	pcap_freecode(&filter);
	pcap_close(handle);
	fclose((FILE*)args);
	return(0);
}
